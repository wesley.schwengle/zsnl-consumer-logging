# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import (
    event_logger_communication,
    event_logger_custom_object,
    event_logger_custom_object_type,
    event_logger_document,
    event_logger_subject,
    event_notifier_case,
    event_notifier_communication,
    event_notifier_document,
)
from .event_logger_admin_catalog import (
    CaseTypeDeleted,
    CaseTypeOnlineStatusChanged,
    CaseTypeVersionUpdated,
    DocumentTemplateLogging,
    EmailTemplateLogging,
    FolderCreated,
    FolderDeleted,
    FolderEntryMoved,
)
from .event_logger_attribute import (
    AttributeCreated,
    AttributeDeleted,
    AttributeEdited,
)
from .event_logger_case import (
    CaseAllocationSet,
    CaseAssigneeSet,
    CaseAttributeUpdated,
    CaseCompletionDateSet,
    CaseCoordinatorSet,
    CaseCreated,
    CaseDestructionDateCleared,
    CaseDestructionDateRecalculated,
    CaseDestructionDateSet,
    CasePaused,
    CaseRegistrationDateSet,
    CaseResultSet,
    CaseResumed,
    CaseStatusSet,
    CaseTargetCompletionDateSet,
)
from .event_logger_case_relation import CaseRelationDeleted
from .event_logger_cm_task import TaskLogger
from .event_logger_objecttype import ObjectTypeDeleted
from .event_logger_subject_relation import (
    SubjectRelationCreated,
    SubjectRelationDeleted,
    SubjectRelationUpdated,
)
from amqpstorm import Message
from json.decoder import JSONDecodeError
from minty.exceptions import ConfigurationConflict
from minty.logging.mdc import mdc
from minty_amqp.consumer import BaseConsumer

EVENT_HANDLERS = {
    "CaseCompletionDateSet": CaseCompletionDateSet(),
    "CaseRegistrationDateSet": CaseRegistrationDateSet(),
    "CaseTargetCompletionDateSet": CaseTargetCompletionDateSet(),
    "CaseDestructionDateCleared": CaseDestructionDateCleared(),
    "CaseDestructionDateSet": CaseDestructionDateSet(),
    "CaseDestructionDateRecalculated": CaseDestructionDateRecalculated(),
    "CaseResultSet": CaseResultSet(),
    "CaseTypeOnlineStatusChanged": CaseTypeOnlineStatusChanged(),
    "FolderEntryMoved": FolderEntryMoved(),
    "CaseAssigneeSet": CaseAssigneeSet(),
    "CustomFieldUpdated": CaseAttributeUpdated(),
    "CaseCoordinatorSet": CaseCoordinatorSet(),
    "CaseStatusSet": CaseStatusSet(),
    "CaseMessagesReassigned": None,
    "AttributeCreated": AttributeCreated(),
    "AttributeEdited": AttributeEdited(),
    "FolderRenamed": None,
    "FolderCreated": FolderCreated(),
    "EmailTemplateCreated": EmailTemplateLogging(),
    "EmailTemplateEdited": EmailTemplateLogging(),
    "CasePaused": CasePaused(),
    "CaseResumed": CaseResumed(),
    "EmailTemplateDeleted": EmailTemplateLogging(),
    "DocumentTemplateCreated": DocumentTemplateLogging(),
    "DocumentTemplateEdited": DocumentTemplateLogging(),
    "DocumentTemplateDeleted": DocumentTemplateLogging(),
    "CaseTypeVersionUpdated": CaseTypeVersionUpdated(),
    "FileCreated": None,
    "FolderDeleted": FolderDeleted(),
    "ObjectTypeDeleted": ObjectTypeDeleted(),
    "CaseTypeDeleted": CaseTypeDeleted(),
    "AttributeDeleted": AttributeDeleted(),
    "CaseCreated": CaseCreated(),
    "ThreadCreated": None,
    "ContactCreated": None,
    "ExternalMessageCreated": event_logger_communication.ExternalMessageCreated(),
    "AttachedToMessage": None,
    "ThreadToCaseLinked": event_logger_communication.ThreadToCaseLinked(),
    "MessageDeleted": event_logger_communication.MessageDeleted(),
    "ThreadDeleted": None,
    "SubjectRelationCreated": SubjectRelationCreated(),
    "SubjectRelationEmailEnqueued": None,
    "SubjectRelationEmailSend": None,
    "CaseAllocationSet": CaseAllocationSet(),
    "SubjectRelationUpdated": SubjectRelationUpdated(),
    "SubjectRelationDeleted": SubjectRelationDeleted(),
    "CaseRelationDeleted": CaseRelationDeleted(),
    "TaskCreated": TaskLogger(),
    "TaskDeleted": TaskLogger(),
    "TaskUpdated": TaskLogger(),
    "TaskCompletionSet": None,  # logging is done in command includes notification.
    "DocumentUpdated": event_logger_document.DocumentUpdated(),
    "DocumentDeleted": event_logger_document.DocumentDeleted(),
    "DocumentAddedToCase": event_logger_document.DocumentAddedToCase(),
    "DocumentCreated": event_logger_document.DocumentCreated(),
    "DocumentAssignmentRejected": event_logger_document.DocumentAssignmentRejected(),
    "NoteCreated": event_logger_communication.NoteCreated(),
    "ContactMomentCreated": event_logger_communication.ContactMomentCreated(),
    "DocumentAssignedToRole": event_logger_document.DocumentAssignedToRole(),
    "DocumentAssignedToUser": event_logger_document.DocumentAssignedToUser(),
    "CustomObjectCreated": event_logger_custom_object.CustomObjectCreated(),
    "CustomObjectUpdated": event_logger_custom_object.CustomObjectUpdated(),
    "CustomObjectRelatedTo": event_logger_custom_object.CustomObjectRelatedTo(),
    "CustomObjectDeleted": event_logger_custom_object.CustomObjectDeleted(),
    "CustomObjectUnrelatedFrom": event_logger_custom_object.CustomObjectUnrelatedFrom(),
    "CustomObjectTypeCreated": event_logger_custom_object_type.CustomObjectTypeCreated(),
    "CustomObjectTypeUpdated": event_logger_custom_object_type.CustomObjectTypeUpdated(),
    "CustomObjectTypeDeleted": event_logger_custom_object_type.CustomObjectTypeDeleted(),
    "BsnRetrieved": event_logger_subject.BsnRetrieved(),
    "PersonUpdated": event_logger_subject.PersonUpdated(),
    "NonAuthenticBsnUpdated": event_logger_subject.NonAuthenticBsnUpdated(),
    "ContactInformationSaved": event_logger_subject.ContactInformationSaved(),
}

NOTIFICATION_HANDLERS = {
    "DocumentAddedToCase": event_notifier_document.DocumentAddedToCase(),
    "DocumentCreated": event_notifier_document.DocumentCreated(),
    "ExternalMessageCreated": event_notifier_communication.ExternalMessageCreated(),
    "NoteCreated": event_notifier_communication.NoteCreated(),
    "ContactMomentCreated": event_notifier_communication.ContactMomentCreated(),
    "DocumentAssignedToUser": event_notifier_document.DocumentAssignedToUser(),
    "SubjectRelationCreated": event_notifier_case.SubjectRelationCreated(),
}


class LegacyLoggingConsumer(BaseConsumer):
    @classmethod
    def get_config(cls, config):
        # This class uses old-style configuration for now

        settings = []
        if "consumer_settings" in config:
            if isinstance(config["consumer_settings"], list):
                settings.extend(config["consumer_settings"])
            else:
                settings.append(config["consumer_settings"])

        for s in settings:
            if (
                s["consumer_class"]
                == "zsnl_amqp_consumers.consumers.LegacyLoggingConsumer"
            ):
                return s

        raise ConfigurationConflict("No configuration found")

    def _register_routing(self):
        self.routing_keys.extend(["zsnl.v2.*.*.*"])

    def __call__(self, message: Message):
        """Handle message to save data to logging table.

        Log records are formatted to be consistent with the Perl system.

        :param BaseConsumer: base consumer
        :type BaseConsumer: BaseConsumer
        :param message: message from amqp broker
        :type message: Message
        """
        logging_info = {
            "routing_key": message.method.get("routing_key", "<unknown>"),
            "message_id": message.message_id,
            "correlation_id": message.correlation_id,
        }

        with mdc(**logging_info):
            self.logger.info("Received message (logging handler)")

            event = self._check_message(message=message)
            if not event:
                return

            event_handler_func = self._get_event_handler(
                event_name=event["event_name"], message=message
            )

            if not event_handler_func:
                self._discard_message(
                    message=message,
                    logline=f"No event Handler for event: {event['event_name']}",
                )
                return

            session = self._get_database_session(
                context=event["context"], message=message
            )
            if not session:
                return

            try:
                log_record = event_handler_func(session=session, event=event)
            except Exception as why:
                # This is pretty much a catch-all: if anything goes wrong,
                # reject the message and have it retry later. Don't crash the
                # service.
                self._reject_and_retry_message(message=message, logline=why)
                return

            if log_record is None:
                # Somenow, no log record was created.
                # Because the code below depends on a log record, we discard
                # the incoming event here and stop processing.
                self._discard_message(
                    message=message,
                    logline="No log line generated for event. Discarding.",
                )
                return

            session.add(log_record)
            session.flush()  # after flush(), log_record object would be automatically
            # assigned with a unique primary key to its id field

            self.__do_notification(message, event, log_record, session)

            session.commit()
            message.ack()

            self.cqrs.infrastructure_factory.flush_local_storage()
            self.logger.info("Handled message successfully.")

    def __do_notification(self, message, event, log_record, session):
        notification_handler_func = self._get_notification_handler(
            event_name=event["event_name"]
        )

        if notification_handler_func:
            try:
                notification_record = notification_handler_func(
                    session=session, event=event, logging_id=log_record.id
                )
            except Exception as why:
                # This is pretty much a catch-all: if anything goes wrong,
                # reject the message and have it retry later. Don't crash the
                # service.
                self._reject_and_retry_message(message=message, logline=why)
                return

            if notification_record:
                session.add(notification_record)
        return

    def _reject_and_retry_message(self, message: Message, logline: str):
        """Reject and retry message.

        :param message: amqp message
        :type message: Message
        :param logline: error message
        :type logline: str
        """
        self.logger.exception(logline)

        message.reject(requeue=False)

        self.cqrs.infrastructure_factory.flush_local_storage()

    def _discard_message(self, message: Message, logline: str):
        """Discard message and remove from queue.

        :param message: amqp message
        :type message: Message
        :param logline: error message
        :type logline: str
        """
        self.logger.warning(f"message_body: {message.body}")
        self.logger.warning(f"Message discarded: {logline}")

        message.ack()

        self.cqrs.infrastructure_factory.flush_local_storage()

    def _check_message(self, message):
        """Check message for errors and discard if corrupted.

        :param message: message
        :type message: Message
        :return: event
        :rtype: dict
        """

        try:
            event = message.json()
        except JSONDecodeError:
            self._discard_message(
                message=message,
                logline="Corrupted message, not able to decode JSON.",
            )
            return

        try:
            event["context"]
            event["event_name"]
        except KeyError as key:
            self._discard_message(
                message=message, logline=f"Incomplete event: missing {key}."
            )
            return

        self.logger.info(f"Received event: {event}")

        return event

    def _get_event_handler(self, event_name: str, message):
        """Get event handler from for corresponding event name.

        :param event_name: name of event
        :type event_name: str
        :param message: message
        :type message: Message
        :return: event handler
        :rtype: function
        """
        try:
            create_record_function = EVENT_HANDLERS[event_name]
            return create_record_function
        except KeyError:
            return

    def _get_database_session(self, context, message):
        """Get session for database

        :param context: context
        :type context: str
        :param message: message
        :type message: Message
        :return: session
        :rtype: Session
        """
        try:
            name = "database"
            session = self.cqrs.infrastructure_factory.get_infrastructure(
                context=context, infrastructure_name=name
            )
            return session
        except ConfigurationConflict as why:
            logline = (
                f"Configuration conflict for context: '{context}'"
                + f" and infrastructure_name: '{name}' conflict_error: {why}"
            )
            self._reject_and_retry_message(message=message, logline=logline)
            return

    def _get_notification_handler(self, event_name: str):
        """Get handler for message notification from for corresponding event name.

        :param event_name: name of event
        :type event_name: str
        :return: event handler
        :rtype: function
        """
        try:
            return NOTIFICATION_HANDLERS[event_name]
        except KeyError:
            return
