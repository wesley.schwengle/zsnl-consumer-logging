# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from json.decoder import JSONDecodeError
from minty.exceptions import ConfigurationConflict, NotFound
from minty_amqp.consumer import BaseConsumer
from unittest import mock
from zsnl_amqp_consumers import (
    consumers,
    event_logger_document,
    event_notifier_document,
)
from zsnl_amqp_consumers.event_logger_base import BaseLogger
from zsnl_amqp_consumers.event_logger_case import CaseTargetCompletionDateSet


class TestConsumers:
    @mock.patch.object(BaseConsumer, "__init__", lambda x: None)
    def setup(self):
        mock_cqrs = mock.MagicMock()
        self.legacy_logging_consumer = consumers.LegacyLoggingConsumer()

        self.legacy_logging_consumer.cqrs = mock_cqrs

    @mock.patch.object(
        CaseTargetCompletionDateSet, "__call__", mock.MagicMock()
    )
    def test_legacy_logging_consumer_call(self):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "registration_date",
                    "new_value": "2019-02-01",
                    "old_value": "2019-01-21",
                }
            ],
            "user_uuid": "9f009a52-39c7-11e9-972c-03e3446c1bb2",
            "created_date": "2019-02-21 14:55:56",
            "event_name": "CaseTargetCompletionDateSet",
            "context": "dev.zaaksysteem.dev",
        }

        self.legacy_logging_consumer(message=mock_message)
        self.legacy_logging_consumer.cqrs.infrastructure_factory.flush_local_storage.assert_called()
        mock_message.ack.assert_called_once()

    def test_legacy_logging_consumer_call_configuration_conflict(self):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "registration_date",
                    "new_value": "2019-02-01",
                    "old_value": "2019-01-21",
                }
            ],
            "user_uuid": "9f009a52-39c7-11e9-972c-03e3446c1bb2",
            "created_date": "2019-02-21 14:55:56",
            "event_name": "CaseTargetCompletionDateSet",
            "context": "dev.zaaksysteem.dev",
        }
        self.legacy_logging_consumer.cqrs.infrastructure_factory.get_infrastructure.side_effect = (
            ConfigurationConflict
        )
        self.legacy_logging_consumer(message=mock_message)

        mock_message.reject.assert_called_once()

    def test_legacy_logging_consumer_not_in_event_mapping(self):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "registration_date",
                    "new_value": "2019-02-01",
                    "old_value": "2019-01-21",
                }
            ],
            "user_uuid": "9f009a52-39c7-11e9-972c-03e3446c1bb2",
            "created_date": "2019-02-21 14:55:56",
            "event_name": "NotInEventMapping",
            "context": "dev.zaaksysteem.dev",
        }

        self.legacy_logging_consumer(message=mock_message)
        self.legacy_logging_consumer.cqrs.infrastructure_factory.flush_local_storage.assert_called()

    def test_legacy_logging_consumer_corrupted_message(self):
        mock_message = mock.MagicMock()
        mock_message.json.side_effect = JSONDecodeError(
            msg="tes", doc="", pos=123
        )

        self.legacy_logging_consumer(message=mock_message)
        mock_message.ack.assert_called_once()

    def test_legacy_logging_consumer_corrupted_message_no_context(self):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "name": "CaseTargetCompletionDateSet"
        }

        self.legacy_logging_consumer(message=mock_message)
        mock_message.ack.assert_called_once()

    @mock.patch.object(
        CaseTargetCompletionDateSet,
        "__call__",
        mock.MagicMock(side_effect=NotFound),
    )
    def test_legacy_logging_consumer_no_result_found(self):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "registration_date",
                    "new_value": "2019-02-01",
                    "old_value": "2019-01-21",
                }
            ],
            "user_uuid": "9f009a52-39c7-11e9-972c-03e3446c1bb2",
            "created_date": "2019-02-21 14:55:56",
            "event_name": "CaseTargetCompletionDateSet",
            "context": "dev.zaaksysteem.dev",
        }

        self.legacy_logging_consumer(message=mock_message)
        self.legacy_logging_consumer.cqrs.infrastructure_factory.flush_local_storage.assert_called()
        mock_message.reject.assert_called_once()

        mock_message.reject.assert_called_once()

    def test_reject_message(self):
        mock_message = mock.MagicMock()
        self.legacy_logging_consumer._reject_and_retry_message(
            message=mock_message, logline="error"
        )
        mock_message.reject.assert_called()

    def test_check_message(self):
        mock_message = mock.MagicMock()
        mock_event = {
            "event_name": "mock_message",
            "context": "message_context",
        }
        mock_message.json.return_value = mock_event
        event = self.legacy_logging_consumer._check_message(mock_message)

        assert mock_event == event

        # missing name
        mock_message = mock.MagicMock()
        mock_event_no_name = {"context": "message_context"}
        mock_message.json.return_value = mock_event_no_name
        event = self.legacy_logging_consumer._check_message(mock_message)
        assert event is None
        mock_message.ack.assert_called()

        # corrupted json
        mock_message = mock.MagicMock()
        mock_message.json.side_effect = JSONDecodeError(
            msg="tes", doc="", pos=123
        )
        event = self.legacy_logging_consumer._check_message(mock_message)
        assert event is None
        mock_message.ack.assert_called()

    def test_get_event_function_not_present(self):
        mock_message = mock.MagicMock()
        func = self.legacy_logging_consumer._get_event_handler(
            event_name="test", message=mock_message
        )
        assert func is None

    def test_get_event_function(self):
        mock_message = mock.MagicMock()
        events = [
            "CaseCompletionDateSet",
            "CaseRegistrationDateSet",
            "CaseTargetCompletionDateSet",
        ]
        for event in events:
            func = self.legacy_logging_consumer._get_event_handler(
                event_name=event, message=mock_message
            )
            assert isinstance(func, BaseLogger)

    def test_get_database_session(self):
        mock_message = mock.MagicMock()
        context = "dev.zaaksysteem.nl"
        self.legacy_logging_consumer.cqrs.infrastructure_factory.get_infrastructure.return_value = (
            "database_session"
        )

        session = self.legacy_logging_consumer._get_database_session(
            context=context, message=mock_message
        )
        assert session == "database_session"

    def test_get_database_session_config_conflict(self):
        mock_message = mock.MagicMock()
        context = "dev.zaaksysteem.nl"
        self.legacy_logging_consumer.cqrs.infrastructure_factory.get_infrastructure.side_effect = (
            ConfigurationConflict
        )

        session = self.legacy_logging_consumer._get_database_session(
            context=context, message=mock_message
        )
        assert session is None
        mock_message.reject.asser_called()

    def test__get_notification_handler(self):
        res = self.legacy_logging_consumer._get_notification_handler(
            "DocumentAddedToCase"
        )
        assert isinstance(res, event_notifier_document.DocumentAddedToCase)

    @mock.patch.object(event_logger_document.DocumentAddedToCase, "__call__")
    @mock.patch.object(event_notifier_document.DocumentAddedToCase, "__call__")
    def test_legacy_logging_consumer_call_with_message_handler(
        self, mock_event_messager, mock_event_logger
    ):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "changes": [
                {"key": "case_uuid", "new_value": "1234", "old_value": None}
            ],
            "user_uuid": "9f009a52-39c7-11e9-972c-03e3446c1bb2",
            "created_date": "2019-02-21 14:55:56",
            "event_name": "DocumentAddedToCase",
            "context": "dev.zaaksysteem.dev",
        }

        self.legacy_logging_consumer(message=mock_message)
        self.legacy_logging_consumer.cqrs.infrastructure_factory.flush_local_storage.assert_called()
        mock_event_messager.assert_called_once()
        mock_event_logger.assert_called_once()

        mock_event_messager.side_effect = Exception
        self.legacy_logging_consumer(message=mock_message)
