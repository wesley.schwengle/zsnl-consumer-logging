# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import requests
from collections import namedtuple
from json.decoder import JSONDecodeError
from minty.exceptions import ConfigurationConflict
from minty_amqp.consumer import BaseConsumer
from unittest import mock
from uuid import uuid4
from zsnl_amqp_consumers.notifications.consumer import (
    EmailNotificationConsumer,
)
from zsnl_amqp_consumers.repository import CaseInformation

SubjectInformation = namedtuple("SubjectInformation", "id display_name uuid")


class TestEmailNotificationConsumer:
    @mock.patch.object(BaseConsumer, "__init__", lambda x: None)
    def setup(self):
        mock_cqrs = mock.MagicMock()
        self.email_notification_cons = EmailNotificationConsumer()

        self.email_notification_cons.cqrs = mock_cqrs

    def test_email_notification_cons_call_configuration_conflict(self):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "content",
                    "new_value": "some content",
                    "old_value": None,
                }
            ],
            "user_uuid": "9f009a52-39c7-11e9-972c-03e3446c1bb2",
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "ExternalMessageCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons.cqrs.infrastructure_factory.get_infrastructure.side_effect = (
            ConfigurationConflict
        )
        self.email_notification_cons(message=mock_message)

        mock_message.reject.assert_called_once()

    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._call_email_notification_handler"
    )
    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.get_case_from_thread"
    )
    def test_consumer_call_document_accepted(self, get_case, call_handler):
        case_uuid = uuid4()
        get_case.return_value = CaseInformation(
            id=123, confidentiality=False, uuid=case_uuid
        )
        user_uuid = uuid4()

        call_handler.return_value = True
        mock_message = mock.MagicMock()

        mock_message.json.return_value = {
            "changes": [
                {"key": "accepted", "new_value": True, "old_value": False},
                {
                    "key": "case_uuid",
                    "new_value": str(case_uuid),
                    "old_value": None,
                },
            ],
            "entity_data": {},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "DocumentAccepted",
            "context": "dev.zaaksysteem.dev",
        }
        event = {
            "changes": [
                {"key": "accepted", "new_value": True, "old_value": False},
                {
                    "key": "case_uuid",
                    "new_value": str(case_uuid),
                    "old_value": None,
                },
            ],
            "entity_data": {},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "DocumentAccepted",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        mock_message["event_name"] = "DocumentAccepted"
        call_handler.assert_called_with(
            event=event, case_uuid=str(case_uuid), context=event["context"]
        )

    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._call_email_notification_handler"
    )
    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.get_case_from_thread"
    )
    def test_consumer_call(self, get_case, call_handler):
        get_case.return_value = CaseInformation(
            id=123, confidentiality=False, uuid=uuid4()
        )
        user_uuid = uuid4()
        call_handler.return_value = True
        mock_message = mock.MagicMock()
        uuid = uuid4()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "new_value": str(uuid),
                    "old_value": None,
                }
            ],
            "entity_data": {},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "ExternalMessageCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        mock_message.ack.assert_called_once()

        mock_message.reset_mock()
        mock_message.json.return_value = {
            "changes": [
                {"key": "case_uuid", "new_value": str(uuid), "old_value": None}
            ],
            "entity_data": {},
            "user_uuid": str(user_uuid),
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "DocumentCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        mock_message.ack.assert_called_once()

    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._discard_message"
    )
    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.EmailNotificationConsumer._call_email_notification_handler"
    )
    @mock.patch(
        "zsnl_amqp_consumers.notifications.consumer.get_case_from_thread"
    )
    def test_consumer_call_request_failed(
        self, get_case, call_handler, discard_message
    ):
        get_case.return_value = CaseInformation(
            id=123, confidentiality=False, uuid=uuid4()
        )
        call_handler.return_value = False
        mock_message = mock.MagicMock()
        uuid = uuid4()
        mock_message.json.return_value = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "new_value": str(uuid),
                    "old_value": None,
                }
            ],
            "entity_data": {},
            "user_uuid": "9f009a52-39c7-11e9-972c-03e3446c1bb2",
            "created_date": "2019-09-24T10:24:46.310185+00:00",
            "event_name": "ExternalMessageCreated",
            "context": "dev.zaaksysteem.dev",
        }
        self.email_notification_cons(message=mock_message)
        discard_message.assert_called_once()

    def test_email_notification_cons_corrupted_message(self):
        mock_message = mock.MagicMock()
        mock_message.json.side_effect = JSONDecodeError(
            msg="tes", doc="", pos=123
        )

        self.email_notification_cons(message=mock_message)
        mock_message.ack.assert_called_once()

    def test_email_notification_cons_corrupted_message_no_context(self):
        mock_message = mock.MagicMock()
        mock_message.json.return_value = {
            "name": "CaseTargetCompletionDateSet"
        }

        self.email_notification_cons(message=mock_message)
        mock_message.ack.assert_called_once()

    def test_reject_message(self):
        mock_message = mock.MagicMock()
        self.email_notification_cons._reject_and_retry_message(
            message=mock_message, logline="error"
        )
        mock_message.reject.assert_called()

    def test_check_message(self):
        mock_message = mock.MagicMock()
        uuid = uuid4()
        mock_event = {
            "event_name": "mock_message",
            "context": "message_context",
            "user_uuid": uuid,
        }
        mock_message.json.return_value = mock_event
        event = self.email_notification_cons._check_message(mock_message)

        assert mock_event == event

        # missing name
        mock_message = mock.MagicMock()
        mock_event_no_name = {"context": "message_context"}
        mock_message.json.return_value = mock_event_no_name
        event = self.email_notification_cons._check_message(mock_message)
        assert event is None
        mock_message.ack.assert_called()

        # corrupted json
        mock_message = mock.MagicMock()
        mock_message.json.side_effect = JSONDecodeError(
            msg="tes", doc="", pos=123
        )
        event = self.email_notification_cons._check_message(mock_message)
        assert event is None
        mock_message.ack.assert_called()

    def test_get_database_session(self):
        mock_message = mock.MagicMock()
        context = "dev.zaaksysteem.nl"
        self.email_notification_cons.cqrs.infrastructure_factory.get_infrastructure.return_value = (
            "database_session"
        )

        session = self.email_notification_cons._get_database_session(
            context=context, message=mock_message
        )
        assert session == "database_session"

    def test_get_database_session_config_conflict(self):
        mock_message = mock.MagicMock()
        context = "dev.zaaksysteem.nl"
        self.email_notification_cons.cqrs.infrastructure_factory.get_infrastructure.side_effect = (
            ConfigurationConflict
        )

        session = self.email_notification_cons._get_database_session(
            context=context, message=mock_message
        )
        assert session is None
        mock_message.reject.asser_called()

    @mock.patch("zsnl_amqp_consumers.notifications.consumer.requests.post")
    def test__call_email_notification_handler_succesfull(self, mock_requests):
        mock_requests().status_code = 200
        mock_requests().json.return_value = {"response": "from_request"}
        event = {"event": "more_event"}
        case_uuid = uuid4()
        context = "testing.zaaksysteem.testing"
        self.email_notification_cons.cqrs.infrastructure_factory.get_config.return_value = {
            "zs_platform_key": "platform_key_1"
        }

        success = (
            self.email_notification_cons._call_email_notification_handler(
                event=event, case_uuid=case_uuid, context=context
            )
        )

        mock_requests.assert_called_with(
            url=f"https://{context}/api/v1/case/{str(case_uuid)}/process_event",
            json=event,
            headers={
                "Content-Type": "application/json",
                "ZS-Platform-Key": "platform_key_1",
            },
            timeout=15,
        )
        assert success is True

    @mock.patch("zsnl_amqp_consumers.notifications.consumer.requests.post")
    def test__call_email_notification_handler_not_succesfull(
        self, mock_requests
    ):

        mock_requests().status_code = 400
        mock_requests().json.return_value = {"response": "from_request"}

        event = {"event": "more_event"}
        case_uuid = uuid4()
        context = "testing.zaaksysteem.testing"
        self.email_notification_cons.cqrs.infrastructure_factory.get_config.return_value = {
            "zs_platform_key": "platform_key_1"
        }

        success = (
            self.email_notification_cons._call_email_notification_handler(
                event=event, case_uuid=case_uuid, context=context
            )
        )

        mock_requests.assert_called_with(
            url=f"https://{context}/api/v1/case/{str(case_uuid)}/process_event",
            json=event,
            headers={
                "Content-Type": "application/json",
                "ZS-Platform-Key": "platform_key_1",
            },
            timeout=15,
        )
        assert success is False

    @mock.patch("zsnl_amqp_consumers.notifications.consumer.requests.post")
    def test__call_email_notification_handler_exception(self, mock_requests):
        mock_requests.side_effect = requests.exceptions.RequestException

        event = {"event": "more_event"}
        case_uuid = uuid4()
        context = "testing.zaaksysteem.testing"
        self.email_notification_cons.cqrs.infrastructure_factory.get_config.return_value = {
            "zs_platform_key": "platform_key_1"
        }

        success = (
            self.email_notification_cons._call_email_notification_handler(
                event=event, case_uuid=case_uuid, context=context
            )
        )

        mock_requests.assert_called_with(
            url=f"https://{context}/api/v1/case/{str(case_uuid)}/process_event",
            json=event,
            headers={
                "Content-Type": "application/json",
                "ZS-Platform-Key": "platform_key_1",
            },
            timeout=15,
        )
        assert success is False

    @mock.patch("zsnl_amqp_consumers.notifications.consumer.requests.post")
    def test__call_email_notification_handler_not_succesfull_invalid_json(
        self, mock_requests
    ):

        mock_requests().status_code = 400
        mock_requests().json.side_effect = ValueError
        event = {"event": "more_event"}
        case_uuid = uuid4()
        context = "testing.zaaksysteem.testing"
        self.email_notification_cons.cqrs.infrastructure_factory.get_config.return_value = {
            "zs_platform_key": "platform_key_1"
        }

        success = (
            self.email_notification_cons._call_email_notification_handler(
                event=event, case_uuid=case_uuid, context=context
            )
        )

        mock_requests.assert_called_with(
            url=f"https://{context}/api/v1/case/{str(case_uuid)}/process_event",
            json=event,
            headers={
                "Content-Type": "application/json",
                "ZS-Platform-Key": "platform_key_1",
            },
            timeout=15,
        )
        assert success is False
